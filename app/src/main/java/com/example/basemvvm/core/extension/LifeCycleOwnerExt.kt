package com.example.basemvvm.core.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */

inline fun <T> LifecycleOwner.observe(data: LiveData<T>, crossinline block: (T?) -> Unit) {
    data.observe(this, { block(it) })
}