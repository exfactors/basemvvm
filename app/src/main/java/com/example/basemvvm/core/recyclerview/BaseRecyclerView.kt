package com.example.basemvvm.core.recyclerview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.basemvvm.R
import com.example.basemvvm.databinding.LayoutBaseRvBinding
import com.example.basemvvm.databinding.LayoutBaseRvEmptyBinding
import com.example.basemvvm.databinding.LayoutBaseRvErrorBinding
import com.example.basemvvm.databinding.LayoutBaseRvShimmerBinding
import com.facebook.shimmer.ShimmerFrameLayout


/**
 * Created by Andri Dwi Utomo on 28/2/2023.
 */
class BaseRecyclerView : FrameLayout {

    private lateinit var mRecyclerView: FrameLayout
    private lateinit var mEmptyView: FrameLayout
    private lateinit var mErrorView: FrameLayout
    private lateinit var mShimmerContainer: ShimmerFrameLayout

    lateinit var baseEmptyBinding: LayoutBaseRvEmptyBinding
    lateinit var baseErrorBinding: LayoutBaseRvErrorBinding
    lateinit var baseShimmerBinding: LayoutBaseRvShimmerBinding
    lateinit var baseRecyclerBinding: LayoutBaseRvBinding
    private var mLayoutShimmer: Int = 0
    private var viewShimmer: View? = null

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initAttrs(attrs)
        initView()
    }

    private fun initAttrs(attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.BaseRecyclerView)
        try {
            mLayoutShimmer = a.getResourceId(R.styleable.BaseRecyclerView_layout_shimmer, R.layout.layout_shimmer)
        } finally {
            a.recycle()
        }
    }

    private fun initView() {
        buildViews()

        // Configure the recycler view
        /*mRecyclerView.apply {
            clipToPadding = mClipToPadding
            if (mPadding != (-1.0f).toInt())
                setPadding(mPadding, mPadding, mPadding, mPadding)
            else
                setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom)
            when (mScrollbar) {
                0 -> isVerticalScrollBarEnabled = false
                1 -> isHorizontalScrollBarEnabled = false
                2 -> {
                    isVerticalScrollBarEnabled = false
                    isHorizontalScrollBarEnabled = false
                }
            }
            if (mScrollbarStyle != -1) scrollBarStyle = mScrollbarStyle
        }*/
    }

    private fun buildViews() {
        mShimmerContainer = ShimmerFrameLayout(context)
        mRecyclerView = FrameLayout(context)
        mEmptyView = FrameLayout(context)
        mErrorView = FrameLayout(context)

        baseEmptyBinding =
            LayoutBaseRvEmptyBinding.inflate(LayoutInflater.from(context), mEmptyView, false)
        baseErrorBinding =
            LayoutBaseRvErrorBinding.inflate(LayoutInflater.from(context), mErrorView, false)
        baseShimmerBinding =
            LayoutBaseRvShimmerBinding.inflate(LayoutInflater.from(context), mShimmerContainer, false)
        baseRecyclerBinding = LayoutBaseRvBinding.inflate(
            LayoutInflater.from(context),
            mRecyclerView,
            false
        )

        addView(baseEmptyBinding.root)
        addView(baseErrorBinding.root)
        addView(baseShimmerBinding.root)
        addView(baseRecyclerBinding.root)
    }

    fun setUpAsList() {
        baseRecyclerBinding.recyclerView.apply {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }
    }

    fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        baseRecyclerBinding.recyclerView.adapter = adapter
    }

    fun showRecycler() {
        hideAll()
        baseRecyclerBinding.root.visibility = View.VISIBLE
    }

    private fun hideAll() {
        viewShimmer?.visibility = View.GONE
        mShimmerContainer.stopShimmer()
        baseRecyclerBinding.root.visibility = View.GONE
        baseEmptyBinding.root.visibility = View.GONE
        baseErrorBinding.root.visibility = View.GONE
    }

    fun showShimmer() {
        hideAll()
        mShimmerContainer.startShimmer()
        if (viewShimmer == null) {
            baseShimmerBinding.viewStub.layoutResource = mLayoutShimmer
            viewShimmer = baseShimmerBinding.viewStub.inflate()
        } else {
            viewShimmer?.visibility = View.VISIBLE
        }
        //dispatch touch
        viewShimmer?.setOnClickListener { _ -> }
    }

    fun stopShimmer() {
        if (mShimmerContainer.isShimmerStarted) {
            mShimmerContainer.stopShimmer()
            viewShimmer?.visibility = View.GONE
        }
    }

    fun showError(title: String? = "", desc: String? = "", btn: Boolean? = true) {
        hideAll()
        baseErrorBinding.root.visibility = View.VISIBLE
        if (title!!.isNotEmpty()) {
            baseErrorBinding.tvTitle.visibility = View.VISIBLE
            baseErrorBinding.tvTitle.text = title
        }
        if (desc!!.isNotEmpty()) {
            baseErrorBinding.tvDesc.visibility = View.VISIBLE
            baseErrorBinding.tvDesc.text = desc
        }
        if (btn == false) {
            baseErrorBinding.btnReload.visibility = View.GONE
        }
    }

    fun reload(listener: OnClickListener) {
        baseErrorBinding.btnReload.setOnClickListener(listener)
    }

    var isRefresh: Boolean = false
        set(value) {
            baseRecyclerBinding.swipeRefresh.isRefreshing = value
        }

    fun setSwipeRefreshListener(listener: SwipeRefreshLayout.OnRefreshListener) {
        baseRecyclerBinding.swipeRefresh.setColorSchemeResources(
            R.color.black_refresh,
            R.color.black_refresh_secondary,
            R.color.white
        )
        baseRecyclerBinding.swipeRefresh.setOnRefreshListener(listener)
    }
}