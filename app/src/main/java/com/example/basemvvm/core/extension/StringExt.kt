package com.example.basemvvm.core.extension

import android.util.Patterns
import android.webkit.URLUtil


/**
 * Created by Andri Dwi Utomo on 20/3/2023.
 */
fun String.isValidEmail(): Boolean {
    return this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String?.isValidPhone(): Boolean {
    val regexPhone = "^(\\+62|62)?[\\s-]?0?8[1-9]{1}\\d{1}[\\s-]?\\d{4}[\\s-]?\\d{2,5}\$".toRegex()

    return (this?.length in 7..13) && (this?.matches(regexPhone) == true)
}

fun String?.isValidUrl(): Boolean {
    return URLUtil.isValidUrl(this)
}