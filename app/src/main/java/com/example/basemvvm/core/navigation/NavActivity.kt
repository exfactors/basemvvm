package com.example.basemvvm.core.navigation

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
abstract class NavActivity : AppCompatActivity() {
    protected abstract val navigationHostId: Int

    protected val navController: NavController
        get() {
            return (supportFragmentManager.findFragmentById(navigationHostId) as NavHostFragment).navController
        }
}