package com.example.basemvvm.core.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
abstract class BaseViewBindingFragment<V : ViewBinding> : BaseFragment() {
    private var _binding: V? = null

    protected val binding: V
        get() = _binding!!

    override fun layoutResourceId(): Int = -1

    protected abstract fun doViewBinding(inflater: LayoutInflater, container: ViewGroup?) : V

    protected open fun doOnCreateView(){}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = doViewBinding(inflater, container)
        doOnCreateView()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}