package com.example.basemvvm.core.extension

import android.app.Activity
import androidx.core.app.ActivityCompat


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */

fun Activity.requestAnyPermission(permission: String, requestCode: Int, showRationale: () -> Unit) {
    requestPermissions(this, permission, requestCode, showRationale)
}

private fun requestPermissions(
    activity: Activity,
    permission: String,
    requestCode: Int,
    showRationale: () -> Unit
) {
    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
        showRationale.invoke()
    }
    ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
}