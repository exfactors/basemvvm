package com.example.basemvvm.core.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes


/**
 * Created by Andri Dwi Utomo on 23/2/2023.
 */
fun ViewGroup.inflate(@LayoutRes layoutResId: Int): View = LayoutInflater.from(context)
    .inflate(layoutResId, this, false)