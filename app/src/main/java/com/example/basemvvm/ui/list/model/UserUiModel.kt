package com.example.basemvvm.ui.list.model


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
interface UserUiModel

data class UserItemModel(
    val title: String,
    val image: String
) : UserUiModel