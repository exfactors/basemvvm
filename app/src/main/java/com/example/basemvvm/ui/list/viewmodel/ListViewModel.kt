package com.example.basemvvm.ui.list.viewmodel

import androidx.lifecycle.viewModelScope
import com.example.basemvvm.core.viewmodel.BaseViewModel
import com.example.basemvvm.data.repository.AppRepository
import com.example.basemvvm.ui.list.model.UserItemModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
@HiltViewModel
class ListViewModel @Inject constructor(
    private val appRepository: AppRepository
) : BaseViewModel<ListViewModel.Event, ListViewModel.State>() {

    sealed class Event {
        data class OnViewCreated(val page: Int, val perPage: Int) : Event()
    }

    sealed class State {
        data class OnViewCreated(val item: List<UserItemModel>, val total: Int) : State()
        object Loading: State()
        object Success: State()
        data class Failed(val message: String): State()
        object ConnectionFailed: State()
    }

    override fun onEventReceived(event: Event) {
        when(event) {
            is Event.OnViewCreated -> {
                getUsers()
            }
        }
    }

    private fun getUsers() {
        pushState(State.Loading)
        viewModelScope.launch {
            appRepository.getUsersApi(1, 20).fetch(
                onSuccess = {
                    var listItem: List<UserItemModel> = listOf()
                    it.data?.map {
                        listItem = listItem + UserItemModel(
                            it.firstName,
                            it.avatar
                        )
                    }
                    pushState(State.OnViewCreated(listItem, it.total))
                },
                onError = {
                    println(it.message)
                    pushState(State.Failed(it.message.toString()))
                },
                connectionFailed = {
                    pushState(State.ConnectionFailed)
                }
            )
        }
    }

}