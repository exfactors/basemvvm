package com.example.basemvvm.ui.multiplelist

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.basemvvm.core.fragment.BaseViewBindingFragment
import com.example.basemvvm.databinding.FragmentMultipleListBinding


/**
 * Created by Andri Dwi Utomo on 15/3/2023.
 */
class MultipleListFragment : BaseViewBindingFragment<FragmentMultipleListBinding>() {
    override fun doViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentMultipleListBinding.inflate(inflater, container, false)


}