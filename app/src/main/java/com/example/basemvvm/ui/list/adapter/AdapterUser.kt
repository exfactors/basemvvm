package com.example.basemvvm.ui.list.adapter

import com.example.basemvvm.R
import com.example.basemvvm.core.rvprovider.itemProviderCreate
import com.example.basemvvm.databinding.ItemListBinding
import com.example.basemvvm.ui.list.model.UserItemModel
import com.example.basemvvm.ui.list.model.UserUiModel


/**
 * Created by Andri Dwi Utomo on 24/2/2023.
 */
class AdapterUser {
    companion object {
        fun onListUserProvider() =
            itemProviderCreate<UserItemModel, UserUiModel, ItemListBinding>(R.layout.item_list) {
                onBind { model, _ ->
                    binding?.model = model
                }
            }
    }
}