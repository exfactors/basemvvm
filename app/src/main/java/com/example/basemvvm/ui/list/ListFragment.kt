package com.example.basemvvm.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.basemvvm.R
import com.example.basemvvm.core.extension.observe
import com.example.basemvvm.core.fragment.BaseViewBindingFragment
import com.example.basemvvm.core.rvprovider.ItemRVProviderAdapter
import com.example.basemvvm.core.rvprovider.itemProviderCreate
import com.example.basemvvm.databinding.FragmentListBinding
import com.example.basemvvm.ui.list.adapter.AdapterUser
import com.example.basemvvm.ui.list.model.UserItemModel
import com.example.basemvvm.ui.list.model.UserUiModel
import com.example.basemvvm.ui.list.viewmodel.ListViewModel


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
class ListFragment : BaseViewBindingFragment<FragmentListBinding>() {

    override fun doViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentListBinding.inflate(inflater, container, false)

    private val viewModel: ListViewModel by viewModels()

    private val adapterUser by lazy {
        ItemRVProviderAdapter(
            AdapterUser.onListUserProvider()
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onEventReceived(ListViewModel.Event.OnViewCreated(1, 10))
        bindViewModel()
        setupRV()
    }

    private fun setupRV() {
        binding.rvGuest.apply {
            setUpAsList()
            setAdapter(adapterUser)
        }
        binding.rvGuest.setSwipeRefreshListener(swipeRefresh)
        binding.rvGuest.reload(reload)
    }

    private val reload = object : View.OnClickListener {
        override fun onClick(p0: View?) {
            viewModel.onEventReceived(ListViewModel.Event.OnViewCreated(1,10))
        }
    }

    private val swipeRefresh = object : SwipeRefreshLayout.OnRefreshListener {
        override fun onRefresh() {
            viewModel.onEventReceived(ListViewModel.Event.OnViewCreated(1, 10))
            binding.rvGuest.isRefresh = false
        }

    }

    private fun bindViewModel() {
        observe(viewModel.state) {
            if (view == null) return@observe
            when (it) {
                is ListViewModel.State.Loading -> binding.rvGuest.showShimmer()
                is ListViewModel.State.Success -> binding.rvGuest.stopShimmer()
                is ListViewModel.State.Failed -> binding.rvGuest.showError(desc = it.message)
                is ListViewModel.State.OnViewCreated -> {
                    binding.rvGuest.showRecycler()
                    adapterUser.update(it.item)
                }
                is ListViewModel.State.ConnectionFailed -> binding.rvGuest.showError(title = getString(R.string.txt_server_internal_error), desc = getString(R.string.txt_failed_to_connect_server))
                else -> {}
            }
        }
    }


}