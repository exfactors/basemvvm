package com.example.basemvvm.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.basemvvm.core.fragment.BaseViewBindingFragment
import com.example.basemvvm.databinding.FragmentDetailBinding


/**
 * Created by Andri Dwi Utomo on 15/3/2023.
 */
class DetailFragment : BaseViewBindingFragment<FragmentDetailBinding>() {
    override fun doViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentDetailBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}