package com.example.basemvvm.app

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
@HiltAndroidApp
class Apps : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
    }
}