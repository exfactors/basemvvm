package com.example.basemvvm.data.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
data class Responses<T> (
    @SerializedName("page")
    val page: Int,

    @SerializedName("per_page")
    val perPage: Int,

    @SerializedName("total")
    val total: Int,

    @SerializedName("total_pages")
    val totalPage: Int,

    @SerializedName("data")
    val data: T? = null
)