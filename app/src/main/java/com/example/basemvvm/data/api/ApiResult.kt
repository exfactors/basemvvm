package com.example.basemvvm.data.api

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
sealed class ApiResult<out T> {
    data class Success<out T>(val value: T) : ApiResult<T>()
    data class Error(val throwable: Throwable) : ApiResult<Nothing>()
    object ConnectionFailed : ApiResult<Nothing>()

    suspend fun fetch(
        onSuccess: suspend (value: T) -> Unit,
        onError: suspend (e: Throwable) -> Unit,
        connectionFailed: suspend () -> Unit
    ) {
        when (this) {
            is Success -> onSuccess.invoke(value)
            is Error -> onError.invoke(throwable)
            is ConnectionFailed -> connectionFailed.invoke()
        }
    }
}

fun <T> ApiResult<T>.asFlow(): Flow<ApiResult<T>> {
    return flow { emit(this@asFlow) }
}