package com.example.basemvvm.data.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
data class Users(
    @SerializedName("id")
    val id: Int,

    @SerializedName("email")
    val email: String,

    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("avatar")
    val avatar: String,
)