package com.example.basemvvm.data.repository

import com.example.basemvvm.data.api.ApiHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
class AppRepository @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun getUsersApi(page: Int, perPage: Int) = withContext(Dispatchers.IO) { apiHelper.getUsers(page, perPage) }
}