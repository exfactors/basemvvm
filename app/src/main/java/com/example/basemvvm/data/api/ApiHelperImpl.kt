package com.example.basemvvm.data.api

import com.example.basemvvm.data.model.Responses
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import javax.inject.Inject


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {
    override suspend fun getUsers(page: Int, perPage: Int) = apiCall { apiService.getUsers(page, perPage) }

    private suspend fun <T> apiCall(call: suspend () -> T): ApiResult<T> {
        return try {
            ApiResult.Success(call.invoke())
        } catch (e: Throwable) {
            when (e) {
                is SocketTimeoutException -> {
                    ApiResult.Error(e)
                }
                is IOException, is ConnectException -> ApiResult.ConnectionFailed
                is HttpException -> {
                    /*return try {
                        val errorStream = e.response()?.errorBody()?.charStream()
                        if (errorStream != null) {
                            val errorResponse = Gson().fromJson(errorStream, Responses::class.java)
                            ApiResult.ApiError(errorResponse.diagnostic)
                        } else {
                            ApiResult.UnknownError(e)
                        }
                    } catch (t: Throwable) {
                        ApiResult.UnknownError(t)
                    }*/
                    ApiResult.Error(e)
                }
                /*is NotFoundSourceException -> {
                    ApiResult.Error(e)
                }*/
                else -> ApiResult.Error(e)
            }
        }
    }
}