package com.example.basemvvm.data.api

import com.example.basemvvm.data.model.Responses
import com.example.basemvvm.data.model.Users
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
interface ApiService {
    @GET("users")
    suspend fun getUsers(@Query("page") page: Int, @Query("per_page") perpage: Int): Responses<List<Users>>
}