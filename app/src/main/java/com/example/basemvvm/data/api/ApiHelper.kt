package com.example.basemvvm.data.api

import com.example.basemvvm.data.model.Responses
import com.example.basemvvm.data.model.Users
import retrofit2.Call


/**
 * Created by Andri Dwi Utomo on 22/2/2023.
 */
interface ApiHelper {
    suspend fun getUsers(page: Int, perPage: Int): ApiResult<Responses<List<Users>>>
}